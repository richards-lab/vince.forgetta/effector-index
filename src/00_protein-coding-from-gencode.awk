# Fetch protein coding genes from GENCODE (build 37/hg19) and obtain NCBI gene id. Output in BED format, name of feature is NCBI Gene id.

zcat $SHARE/DATA/GENCODE/gencode.v29lift37.annotation.gtf.gz | \
     awk 'FNR==NR { ensg[$3]=$2 ;next} $0 !~ /^#/ && $3 == "gene" && $0 ~ /gene_type \"protein_coding\"/ { match($0, /gene_name \"(\S+)\"/, gn); match($0, /gene_id \"(ENSG[0-9]+)/, gi); OFS="\t"; if (gi[1] in ensg) { print $1, $4, $5, ensg[gi[1]]","gi[1]","gn[1] } }' \
     <(zcat $SHARE/DATA/NCBI/GENE/gene2ensembl.gz) - \
     | sort -k1,1 -k2,2n \
     > gencode29b37_protein_coding.ncbi_gene_id.bed

zcat $SHARE/DATA/GENCODE/gencode.v29lift37.annotation.gtf.gz | \
     awk 'FNR==NR { ensg[$3]=$2 ;next} $0 !~ /^#/ && $3 == "gene" && $0 ~ /gene_type \"protein_coding\"/ { match($0, /gene_name \"(\S+)\"/, gn); match($0, /gene_id \"(ENSG[0-9]+)/, gi); OFS="\t"; if (gi[1] in ensg) { print $1, $4, $5, ensg[gi[1]]","gi[1]","gn[1],0,$7 } }' \
     <(zcat $SHARE/DATA/NCBI/GENE/gene2ensembl.gz) - \
     | sort -k1,1 -k2,2n \
     > gencode29b37_protein_coding.ncbi_gene_id.bed

awk '{ pos=$2 } $6=="-" { pos=$3 } { OFS="\t"; print $1,pos,pos,$4,$5,$6 }' gencode29b37_protein_coding.ncbi_gene_id.bed | sort -k1,1 -k2,2n > gencode29b37_protein_coding.ncbi_gene_id.geneStart.bed



zcat $SHARE/DATA/GENCODE/gencode.v29lift37.annotation.gtf.gz | \
     awk 'FNR==NR { ensg[$3]=$2 ;next} $0 !~ /^#/ && $3 == "gene" && $0 ~ /gene_type \"protein_coding\"/ { match($0, /gene_name \"(\S+)\"/, gn); match($0, /gene_id \"(ENSG[0-9]+)/, gi); OFS="\t"; if (gi[1] in ensg) { print $1, $4, $5, ensg[gi[1]] } }' \
     <(zcat $SHARE/DATA/NCBI/GENE/gene2ensembl.gz) - \
     | sort -k1,1 -k2,2n \
     > gencode29b37_protein_coding.ncbi_gene_id_only.bed


      
