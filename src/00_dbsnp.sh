

wget -O scratch/00_resources/snp151.sql http://hgdownload.soe.ucsc.edu/goldenPath/hg19/database/snp151.sql
wget -O scratch/00_resources/snp151.txt.gz http://hgdownload.soe.ucsc.edu/goldenPath/hg19/database/snp151.txt.gz
{ sqlite3 -separator ' ' "$SHARE/DATA/UCSC/hg19/database/snp150_allCols.db" "select * from snp150 where name=\"$snp\"" } \
    | awk '$16 ~ /missense/ { OFS="\t"; print $2, $3, $4, $5 }' \
    | sort -k1,1 -k2,2n \
    > missense_snps.bed
