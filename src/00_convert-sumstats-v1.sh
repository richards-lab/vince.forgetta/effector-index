trait=$1
N=$2

mkdir -p scratch/01_training/${trait}/01_finemap-gwas-sumstats/

awk -v N=$N 'BEGIN { print "SNP A1 A2 FREQ BETA SE P N CHR POS" } { print $1,$4,$5,$6,$7,$8,$9,N,$2,$3 }' \
    <(tail -n +2 /mnt/RICHARDS_JBOD1/home/vince.forgetta/work/20181002-GWAS-CAUSAL-GENES/data/traits/${trait}/gwas.assoc) \
    > scratch/01_training/${trait}/01_finemap-gwas-sumstats/gwas.ma
