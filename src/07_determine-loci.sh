#!/bin/bash

trait=$1

workdir=scratch/01_training/${trait}/01_finemap-gwas-sumstats
statsfile=${workdir}/gwas.ma.cojo.labf10.txt
lradius=500000
maxleadsnps=10


# Format lead SNPs to BED format
tail -n+2 ${statsfile} \
    | awk '{ OFS="\t"; print "chr"$1, $3, $3+1, $2, $17 }' \
    | sort -k1,1 -k2,2n \
    > ${workdir}/gwas-lead-snps.bed


# Merge adjacent lead SNPs to determine loci
bedtools merge -i ${workdir}/gwas-lead-snps.bed \
	 -d ${lradius} -o collapse,count -c 4 \
	 > ${workdir}/gwas-lead-snps-merged.bed

# Add paddingto each locus
awk -v r=$lradius -v maxleadsnps=$maxleadsnps \
    '{ lc+=1; c=$1; e=$3; s=$2; ; s=s-r; e=e+r; if(s<0) { s=1 }; printf "%s\t%d\t%d\tL%s.%d.%d\t%s\n",c,s,e,substr(c,4),s/1000,e/1000,$5 }' \
    ${workdir}/gwas-lead-snps-merged.bed \
    > ${workdir}/gwas-loci.bed


# Intersect lead SNPs back to each locus
bedtools intersect -a ${workdir}/gwas-loci.bed -b ${workdir}/gwas-lead-snps.bed -wa -wb | awk 'BEGIN { OFS="\t" } { print $6,$7,$8,$4","$9,$10,"+" }' > ${workdir}/gwas-lead-snps-by-locus.bed

# Find genes that overlap each locus
bedtools intersect -a ${workdir}/gwas-loci.bed \
	 -b scratch/00_resources/protein-coding-genes.txt \
	 -wa -wb -F 0.5 >u ${workdir}/genes-at-loci.txt

# Format genes at loci to BED format
awk '{ OFS="\t"; print $6,$7,$8,$9","$4,$10,$11 }' ${workdir}/genes-at-loci.txt | sort -k1,1 -k2,2n > ${workdir}/genes-at-loci.bed

# Format TSS of genes at loci to BED format
awk '$6=="+" { tss=$2 } $6=="-" { tss=$3 } { OFS="\t"; print $1,tss,tss,$4,$5,$6 }' ${workdir}/genes-at-loci.bed | sort -k1,1 -k2,2n > ${workdir}/tss-of-genes-at-loci.bed
