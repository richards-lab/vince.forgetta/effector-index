#!/bin/bash
wget -O scratch/00_resources/gene2ensembl.gz ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2ensembl.gz 
wget -O scratch/00_resources/gencode.v35lift37.basic.annotation.gtf.gz ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_35/GRCh37_mapping/gencode.v35lift37.basic.annotation.gtf.gz 
awk -f src/00_filter-gencode.awk \
    <(zcat scratch/00_resources/gene2ensembl.gz) \
    <(zcat scratch/00_resources/gencode.v35lift37.basic.annotation.gtf.gz) \
    | sort -k4,4 -k7,7n -k5,5nr \
    | sort -k 4,4 -u \
    | sort -k1,1 -k2,2n \
    > scratch/00_resources/protein-coding-genes.txt
