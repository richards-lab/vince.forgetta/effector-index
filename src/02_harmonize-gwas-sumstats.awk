################################################################################
## First input file: Index of SNP in ref panel, 2 columns: SNPID and CHR:POS:A1:A2.
## Index is meant to convert CHR:POS:A1:A2 to the SNPID present in reference panel,
## as SNPs are identified by SNPID by next step in the process.

## Second input file: GWAS summary statistics.
## Expect input as file with columns in the following order:
## SNP A1 A2 FREQ BETA SE P N CHR POS
## Header is expected but skipped.


BEGIN {
    comp["A"]="T"
    comp["a"]="T"
    comp["C"]="G"
    comp["c"]="G"
    comp["G"]="C"
    comp["g"]="C"
    comp["T"]="A"
    comp["t"]="A"
}

## Load reference panel index keyed by CHR:POS:A1:A2 with value of SNPID.
FNR==NR { m[$2]=$1; next }
## Skip first line in the sumstats file.
FNR==1 { next }
## Skip rare SNPs
$4 > 0.995 || $4 < 0.005 { s+=1; next }
##   1  2  3    4    5  6 7 8   9  10
## SNP A1 A2 FREQ BETA SE P N CHR POS
## Replace the SNPID in the GWAS with the SNPID in the ref panel using CHR:POS:A1:A2 as key.
($9":"$10":"$2":"$3 in m) { $1=m[$9":"$10":"$2":"$3]; print $0; next }
($9":"$10":"$3":"$2 in m) { $1=m[$9":"$10":"$3":"$2]; print $0; next }
($9":"$10":"comp[$2]":"comp[$3] in m) { $1=m[$9":"$10":"comp[$2]":"comp[$3]]; print $0; next }
($9":"$10":"comp[$3]":"comp[$2] in m) { $1=m[$9":"$10":"comp[$3]":"comp[$2]]; print $0; next }
