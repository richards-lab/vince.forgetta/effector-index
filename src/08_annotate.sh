
trait=$1
workdir=scratch/01_training/${trait}/01_finemap-gwas-sumstats

## 1. Gene/SNP overlaps
bedtools intersect -a ${workdir}/genes-at-loci.bed -b ${workdir}/gwas-lead-snps-by-locus.bed -wa -wb > ${workdir}/gene-snp-overlaps.bed

# 2. Closest SNP to gene
bedtools closest -a ${workdir}/tss-of-genes-at-loci.bed -b ${workdir}/gwas-lead-snps-by-locus.bed -wa -wb -d > ${workdir}/gene-snp-closest.bed


## !! SKIP FOR NOW NEEDS REFACTORING !! 
# 3. Annotate FINEMAP SNPs for functional consequence in protein coding gene using dbSNP
## TODO: REWRITE THIS TO INCLUDE HOW FILE IS GENERATED
##bedtools intersect -f 1 -r -a /mnt/RICHARDS_JBOD1/SHARE/DATA/UCSC/hg19/database/snp150_func.bed -b ${workdir}/gwas-lead-snps-by-locus.bed -wa -wb > ${workdir}/snp-dbsnp.txt
##awk '$9 ~ $4 || $4 !~ /rs/ { OFS="\t"; print $1,$2,$3,$9,$5,"+" }' snp-dbsnp.txt > snp-dbsnp.bed
##bedtools intersect -a ${workdir}/genes-at-loci.bed -b snp-dbsnp.bed -wa -wb > gene-snp-dbsnp.bed

# 4. Annotate FINEMAP SNPs for functional consequence in protein coding gene using SNPEff
awk 'BEGIN { OFS="\t"; print "#CHROM","POS","ID","REF","ALT","QUAL","FILTER" } FNR==NR { ref[$1]=$2; alt[$1]=$3;next } { split($4,a,","); print $1,$2,$4,ref[a[2]],alt[a[2]],".","." }' \
    ${workdir}/gwas.ma \
    <(tail -n +2 ${workdir}/gwas-lead-snps-by-locus.bed) \
    > ${workdir}/gwas-lead-snps-by-locus.vcf

java -Xmx4g -jar bin/snpEff/snpEff.jar -v GRCh37.75 ${workdir}/gwas-lead-snps-by-locus.vcf > ${workdir}/gwas-lead-snps-by-locus-snpeff.vcf 2> ${workdir}/gwas-lead-snps-by-locus-snpeff.log
grep -v '^#' ${workdir}/gwas-lead-snps-by-locus-snpeff.vcf | awk '{ split($8,a,","); for (i=1;i<=length(a);i+=1) { split(a[i],b,"|"); OFS="\t"; print $3,b[3],b[4],b[5] }}' | sort | uniq > ${workdir}/gwas-lead-snps-by-locus-snpeff-impact.txt
awk 'BEGIN { FN=0 } FNR==1 { FN++ } FN==1 { snps[$4]=$0 } FN==2 { split($4,a,","); genes[a[3]]=$0 } FN==3 && ($3 in genes){ OFS="\t"; split(snps[$1],a,"\t"); print genes[$3], a[1],a[2],a[3],a[4],$2,a[6] }' \
    ${workdir}/gwas-lead-snps-by-locus.bed  \
    ${workdir}/genes-at-loci.bed \
    ${workdir}/gwas-lead-snps-by-locus-snpeff-impact.txt \
    > ${workdir}/gwas-lead-snps-by-locus-snpeff-impact-with-gene-info.bed

if [ -s "${workdir}/gwas-lead-snps-by-locus-snpeff-impact-with-gene-info.bed" ]
then
    awk '{ OFS="\t"; print $1,$2,$3,$10":"$4,$11 }' ${workdir}/gwas-lead-snps-by-locus-snpeff-impact-with-gene-info.bed | sort -k1,1 -k2,2n | bedtools groupby -o distinct -g 1-4 -c 5 > ${workdir}/gwas-lead-snps-by-locus-snpeff-impact-with-gene-info-distinct.bed
else
    touch ${workdir}/gwas-lead-snps-by-locus-snpeff-impact-with-gene-info-distinct.bed 
fi

# 5. Annotate FINEMAP SNPs for overlap with one or more openchromatin peaks
#bedtools intersect -b ../../MAURANO-DHS/${TRAIT}_dhs.bed -a ${FMP}.bed -wa -wb > ${FMP}_opchr.txt
#awk '{ OFS="\t"; print $1,$2,$3,$4,$10,"+" }' ${FMP}_opchr.txt > ${FMP}_openchromfull.txt
#bedtools groupby -i ${FMP}_openchromfull.txt -o count_distinct -g 1-4 -c 5 | awk '{ OFS="\t"; print $1,$2,$3,$4,$5,"+" }' > ${FMP}_openchromgrp.txt
#bedtools closest  -a loci_tss.bed -b ${FMP}_openchromgrp.txt -wa -wb > ${FMP}_openchrom.bed
#bedtools closest -t first -b loci_tss.bed -a ${FMP}_openchromgrp.txt -wa -wb > ${FMP}_openchromsnp.tmp
#paste <(cut -f 7-12 ${FMP}_openchromsnp.tmp) <(cut -f 1-6 ${FMP}_openchromsnp.tmp) > ${FMP}_openchromsnp.bed


bedtools intersect -b scratch/00_resources/ENCODE_REMC_FLER_T2D_2019aug.flt.hotspots2.fdr0.05.hg19.bed -a ${workdir}/gwas-lead-snps-by-locus.bed -wa -wb > ${workdir}/gwas-lead-snps-by-locus-alldhs.txt
awk '{ OFS="\t"; print $1,$2,$3,$4,$10,"+" }' ${workdir}/gwas-lead-snps-by-locus-alldhs.txt > ${workdir}/gwas-lead-snps-by-locus-alldhs.bed
bedtools groupby -i ${workdir}/gwas-lead-snps-by-locus-alldhs.bed -o count_distinct -g 1-4 -c 5 | awk '{ OFS="\t"; print $1,$2,$3,$4,$5,"+" }' > ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped.txt
bedtools closest  -a ${workdir}/tss-of-genes-at-loci.bed -b ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped.txt -wa -wb > ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped-closest-to-gene.bed
bedtools closest -t first -b ${workdir}/tss-of-genes-at-loci.bed -a ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped.txt -wa -wb > ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped-closest-to-snp.txt
paste <(cut -f 7-12 ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped-closest-to-snp.txt) <(cut -f 1-6 ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped-closest-to-snp.txt) > ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped-closest-to-snp.bed


#6. Annotate FINEMAP SNPs for GTEx eQTL in trait-matched tissues
#bedtools intersect -a ../../MAURANO-GTEx/${TRAIT}_gtex.bed -b ${FMP}.bed -wa -wb > ${FMP}_gtex.txt
#awk 'FNR==NR { split($4,a,","); m[a[1]","a[2]","a[3]]=$4; next } { OFS="\t"; $4=m[$4]; print $0 }' loci_genes.bed ${FMP}_gtex.txt > ${FMP}_gtex.bed

echo -e "snp.chrom\tsnp.chrom.start\tsnp.chrom.end\tsnp.key\tsnp.log10bf\tsnp.strand" > ${workdir}/snp_masterlist.txt
awk 'FNR==NR { m[$10]=$0;next; } $4 in m { print }' \
    <(cat ${workdir}/gene-snp-overlaps.bed \
	  ${workdir}/gene-snp-closest.bed \
	  ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped-closest-to-snp.bed \
	  ${workdir}/gwas-lead-snps-by-locus-snpeff-impact-with-gene-info-distinct.bed \
	  ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped-closest-to-gene.bed \
	  ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped-closest-to-snp.bed) \
    ${workdir}/gwas-lead-snps-by-locus.bed \
    | sort | uniq >> ${workdir}/snp_masterlist.txt

echo -e "snp.key\tsnp.locus\tsnp.name\tgene.key\tgene.gid\tgene.eid\tgene.name\tgene.locus" > ${workdir}/gene_masterlist.txt
awk -F '\t' '$10 != "." && $10 != "" && $4 != "" { OFS="\t"; split($10,b,","); split($4,a,","); split(m[$10],c,"\t"); print $10, b[1], b[2], $4, a[1], a[2], a[3], a[4] }' \
    <(cat ${workdir}/gene-snp-overlaps.bed \
          ${workdir}/gene-snp-closest.bed \
          ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped-closest-to-snp.bed \
          ${workdir}/gwas-lead-snps-by-locus-snpeff-impact-with-gene-info-distinct.bed \
          ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped-closest-to-gene.bed \
          ${workdir}/gwas-lead-snps-by-locus-alldhs-grouped-closest-to-snp.bed) \
    | sort | uniq >> ${workdir}/gene_masterlist.txt
