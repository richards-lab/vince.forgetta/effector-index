#!/bin/bash
#PBS -o logs/
#PBS -e logs/
#PBS -l walltime=4:00:00
#PBS -l nodes=1:ppn=4
#PBS -l mem=24G
#PBS -l vmem=24G
#PBS -l epilogue=/mnt/RICHARDS_JBOD1/SHARE/bin/epilogue.sh

cd $PBS_O_WORKDIR

mkdir -p logs/

## Script requires arguments: -v trait="trait-abbrev" -t chrom-range
chrom=${PBS_ARRAYID}
./src/04_select-lead-snps.sh $trait $chrom
