# Harmonize GWAS used for model training

Goal is to generate input file format for GCTA-COJO with following columns:

    SNP A1 A2 FREQ BETA SE P N CHR POS
	
Order of the first 8 columns needs to be maintained as GCTA-COJO does not use header to infer content.
 
The following sub-sections performs this task for the traits traits used in model training:

## Low-density lipoprotein (abbrev: ldl)

	trait="ldl"
	workdir=scratch/01_training/${trait}/01_finemap-gwas-sumstats/
	GWAS_F=${workdir}/gwas_ldl.fastGWA.fzp
    echo "SNP A1 A2 FREQ BETA SE P N CHR POS" > ${workdir}/gwas.ma
    awk 'FNR==NR { m[$2]=$1; next } $7 > 0.995 || $7 < 0.005 { next } ($1":"$3":"$4":"$5 in m) { OFS=" "; print m[$1":"$3":"$4":"$5],$4,$5,$7,$8,$9,$10,$6,$1,$3; next } ($1":"$3":"$5":"$4 in m) { OFS=" "; print m[$1":"$3":"$5":"$4],$4,$5,$7,$8,$9,$10,$6,$1,$3 }' \
    <(zcat scratch/00_resources/ukb-bycroft-rand5k-snp-index.txt.gz) \
    ${GWAS_F} \
    >> ${workdir}/gwas.ma
	./src/03_serialize-gwas-sumstats.R ldl

## Rheumatoid arthritis (abbrev: ra)

	trait="ra"
	workdir=scratch/01_training/${trait}/01_finemap-gwas-sumstats/
	mkdir -p ${workdir}
	GWASF=${workdir}/MegaGWAS_summary_European.txt.gz-2019-3-3-9.57.45.txt
	
	(echo "SNP A1 A2 FREQ BETA SE P N CHR POS";
	tail -n+2 ${GWASF} | awk '{ print $1,$4,$5,$10,$11,$12,$13,$7+$8,$3,$2 }') > ${workdir}/gwas.txt
	
	awk -f src/02_harmonize-gwas-sumstats.awk <(zcat scratch/00_resources/ukb-bycroft-rand5k-snp-index.txt.gz) ${workdir}/gwas.tmp > ${workdir}/gwas.ma
    ./src/03_serialize-gwas-sumstats.R ra
	
##	trait="ra"
##	workdir=scratch/01_training/${trait}/01_finemap-gwas-sumstats/
##	mkdir -p ${workdir}	
##	GWASF=${workdir}/MegaGWAS_summary_European.txt.gz-2019-3-3-9.57.45.txt
##	echo "SNP A1 A2 FREQ BETA SE P N CHR POS" > ${workdir}/gwas.ma
##    awk 'FNR==NR { m[$2]=$1; next } $10 > 0.995 || $10 < 0.005 { s+=1; next } \
##		($3":"$2":"$4":"$5 in m) { OFS=" "; print m[$3":"$2":"$4":"$5],$4,$5,$10,$11,$12,$13,$7+$8,$3,$2 } \
##		($3":"$2":"$5":"$4 in m) { OFS=" "; print m[$3":"$2":"$5":"$4],$4,$5,$10,$11,$12,$13,$7+$8,$3,$2 } END { print s > "/dev/stderr" }' \
##    <(zcat scratch/00_resources/ukb-bycroft-rand5k-snp-index.txt.gz) \
##    ${GWASF} \
##    >> ${workdir}/gwas.ma

## Estimated bone-mineral density (abbrev: ebmd)

	trait="ebmd"
	workdir=scratch/01_training/${trait}/01_finemap-gwas-sumstats/
	GWASF=${workdir}/Biobank2-British-Bmd-As-C-Gwas-SumStats.txt.gz
    echo "SNP A1 A2 FREQ BETA SE P N CHR POS" > ${workdir}/gwas.ma


#     1      2      3      4       5        6       7        8       9    10     11        12       13   14
# SNPID   RSID    CHR     BP      EA      NEA     EAF     INFO    BETA    SE      P       P.I     P.NI    N

	(echo "SNP A1 A2 FREQ BETA SE P N CHR POS";
	zcat ${GWASF} | tail -n+2 | awk '{ print $2,$5,$6,$7,$9,$10,$13,$14,$3,$4 }') > gwas.txt
	
	
	
	awk 'FNR==NR { m[$2]=$1; next } $7 > 0.995 || $7 < 0.005 { next } ($1":"$3":"$4":"$5 in m) { OFS=" "; print m[$1":"$3":"$4":"$5],$4,$5,$7,$8,$9,$10,$6,$1,$3; next } ($1":"$3":"$5":"$4 in m) { OFS=" "; print m[$1":"$3":"$5":"$4],$4,$5,$7,$8,$9,$10,$6,$1,$3 }' \
    
	
	<(zcat scratch/00_resources/ukb-bycroft-rand5k-snp-index.txt.gz) \
    ${GWAS_F} \
    >> ${workdir}/gwas.ma
	
	./src/03_serialize-gwas-sumstats.R ebmd


-sum
echo "SNP CHR BP A1 A2 FRQ BETA SE P" > gwas.assoc
echo "rsid chromosome position allele1 allele2 eaf beta se p" > gwas.txt
awk 'FNR==NR { m[$2]=$1; next } $7 > 0.995 || $7 < 0.005 { next } ($3":"$4":"$5":"$6 in m) { OFS=" "; print m[$3":"$4":"$5":"$6],$3,$4,$5,$6,$7,$9,$10,$13; next } ($3":"$4":"$6":"$5 in m) { OFS=" "; print m[$3":"$4":"$6":"$5],$3,$4,$5,$6,$7,$9,$10,$13 }' \
    
