# Build LD reference panel

Select 50,000 random individuals from "in.white.British.ancestry.subset" in Bycroft et al. (2018), PMID: 30305743.

    awk '$23==1 { print $1, $1 }' ukb#####_sqc_v2.txt > in.white.British.ancestry.subset.txt
    awk 'FNR==NR { m[$1]=$1; next } $1 in m { print $1, $1 }' \
		ukb#####_imp_chr1_v3_s487395.sample \
		in.white.British.ancestry.subset.txt \
		> in.white.British.ancestry.subset.imputed.txt
    sort -R --random-source=in.white.British.ancestry.subset.imputed.txt \
        in.white.British.ancestry.subset.imputed.txt \
        | head -n 5000 \
	> rand.5k.FID_IID.txt

Note that this will select a fixed random subset based on the contents of the source file, therefore, re-running this command will result in the same random subset.

Extract SNPs from UK Biobank version 3 imputed data, removing duplicates because plink doesn't handle them:x

	# PBS_ARRAYID is variable storing chromosome number
	cut -f 2 ukb_mfi_chr${PBS_ARRAYID}_v3.txt | sort | uniq -d > ${PBS_ARRAYID}.dups
	
    plink2 --bgen ukb_imp_chr${PBS_ARRAYID}_v3.bgen \
		--sample ukb#####_imp_chr1_v3_s487395.sample \
		--keep rand.5k.FID_IID.txt \
		--make-bed \
		--out ${PBS_ARRAYID} \
		--threads 8 \
		--memory 40000 \
		--exclude ${PBS_ARRAYID}.dups

Lastly, create SNP index of containing columns "SNP ID" and "Chromosome:position:allele1:allele2" that will be used to harmonize GWAS sumstats

    awk '{ print $2, $1":"$4":"$5":"$6 }' *.bim | gzip -c > scratch/00_resources/ukb-bycroft-rand5k-snp-index.txt.gz
