FNR==NR { ensg[$3]=$2 ; next}
$0 !~ /^#/ && \
$3 == "transcript" && \
$0 ~ /gene_type \"protein_coding\"/ && \
$0 ~ / level (1|2|3|4);/ && \
$0 ~ /transcript_type \"protein_coding\"/ {
    match($0, /level ([0-9])/, level); match($0, /gene_name \"(\S+)\"/, gn); match($0, /gene_id \"(ENSG[0-9]+)/, gi); OFS="\t"; if (gi[1] in ensg) { glen=$5-$4;print $1, $4, $5, ensg[gi[1]]","gi[1]","gn[1],glen,$7,level[1] }
}
