#!/bin/bash
# Run GCTA-COJO
# The following command uses the reference panel and GWAS sumstats created previously to find independant signals (.jma.cojo files), ie. "lead SNPs", and using these lead SNPs conditions all surrounding sumstats (.cma.cojo files).

trait=$1
chrom=$2

ldpaneldir=~/projects/richards/restricted/ukb-general/scratch/genetic-data/genome/imputed.v3/plink.bycroftRand5k
./bin/gcta64 --cojo-file scratch/01_training/${trait}/01_finemap-gwas-sumstats/gwas.ma \
	     --cojo-slct \
	     --cojo-p 5e-8 \
	     --cojo-wind 5000 \
	     --cojo-collinear 0.9 \
	     --threads 8 \
	     --bfile ${ldpaneldir}/${chrom} \
	     --chr ${chrom} \
	     --out scratch/01_training/${trait}/01_finemap-gwas-sumstats/gwas.ma.${chrom}
